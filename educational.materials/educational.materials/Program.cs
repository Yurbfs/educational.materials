﻿using System;

namespace educational.materials
{
    class Program
    {
        static void Main(string[] args)
        {
            string hellowolf = "Привет Волк";
            int height = 80;
            byte weight = 250;
            sbyte vision = -8;
            short age = 24;
            ushort pawlength = 655;
            int earlength = 15;
            long noselength = 11;
            ulong numberofpaws = 50;
            float taillength = 166.35F;
            double numberoffangs = 190.70;
            decimal woollength = 15.80M;
            char firstletterofthename = 'W';
            bool alivewolf = true;
            object byewolf = "Пока Волк";
            Console.WriteLine(hellowolf);
            Console.WriteLine($"Рост: {height}");
            Console.WriteLine($"Вес: {weight}");
            Console.WriteLine($"Зрение: {vision}");
            Console.WriteLine($"Возраст: {age}");
            Console.WriteLine($"Длина лапы: {pawlength}");
            Console.WriteLine($"Длина уха: {earlength}");
            Console.WriteLine($"Длина носа: {noselength}");
            Console.WriteLine($"Количество лап: {numberofpaws}");
            Console.WriteLine($"Длина хвоста: {taillength}");
            Console.WriteLine($"Количество клыков: {numberoffangs}");
            Console.WriteLine($"Длина шерсти: {woollength}");
            Console.WriteLine($"Первая буква имени: {firstletterofthename}");
            Console.WriteLine($"Волк живой: {alivewolf}");
            Console.WriteLine(byewolf);

           
            
        }

        
    }
}
